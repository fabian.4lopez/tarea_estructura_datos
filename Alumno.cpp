#include "Alumno.h"
#include <iostream>
#include <string>

Alumno::Alumno() :name(""), apellido("")
{
}

Alumno::Alumno(std::string name, std::string apellido) :
    name(name)
    , apellido(apellido)
{
}

std::string Alumno::getname() const
{
    return name;
}

void Alumno::setname(std::string Name)
{
    name  = Name;
}

std::string Alumno::getapellido() const
{
    return apellido;
}


