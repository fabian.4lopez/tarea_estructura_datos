#include "Alumno.h"
#include "Nodo.h"
#include "pila.h"
#include "Main.h"
#include <iostream>
#include "PiladeAlumnos.h"

int main() {
	Alumno Pa = Alumno("Fabian", "Lopez");
	Alumno Pb = Alumno("Fabian", "Lopez");
	Alumno Pc = Alumno("Fabian", "Lopez");
	Alumno Pd = Alumno("Fabian", "Lopez");

	PiladeAlumnos mystack = PiladeAlumnos();
	mystack.Push(Pa);
	std::cout << Pa.Peek().getname() << std::endl;

	return 0;


}