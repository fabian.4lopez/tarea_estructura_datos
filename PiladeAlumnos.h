
class ListaVaciaException {};

class Nodo {
public:
	Nodo* Siguiente;
	Alumno Valor;
	Nodo();
	Nodo(const Alumno& alumno);
};

class PiladeAlumnos
{
public:
	Nodo *Cabeza;
public:
	PiladeAlumnos();
	
	void Push(const Alumno& alumno);
	Alumno Pop();
	Alumno Peek();
	bool PilaVacia() const;
	
};



