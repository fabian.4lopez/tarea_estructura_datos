#pragma once
#include "Alumno.h"
#include "pila.h"
#include "Main.h"
#include <iostream>
class Nodo {
public:
    Nodo* Siguiente;
    Alumno Valor;
    Nodo();
    Nodo(const Alumno& alumno);
};