#include "ColaPrioridad.h"

ColaPrioridad::ColaPrioridad()
{
    ListaPasajeros = new LinkedList();
}

void ColaPrioridad::EnColar(Pasajero& const pasajero)
{
    ListaPasajeros->InsertarFinal(pasajero);
}

Pasajero ColaPrioridad::DesColar()
{
    return ListaPasajeros->RemoverPrimero();
}


bool ColaPrioridad::ColaVacia()
{
    return ListaPasajeros->ListaVacia();

}








