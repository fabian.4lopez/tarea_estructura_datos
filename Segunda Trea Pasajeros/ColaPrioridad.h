#pragma once
#include "Pasajero.h"
#include <iostream>
#include "Lista.h"


class ColaPrioridad
{
public:
    LinkedList* ListaPasajeros;
public:
    ColaPrioridad();
    void EnColar(Pasajero& const pasajero);
    Pasajero DesColar();
    bool ColaVacia();
    
};
