
#include <iostream>
#include "lista.h"
#include <sstream>



LinkedList::LinkedList() :
    Cabeza(nullptr) {
}

void LinkedList::InsertarFinal(Pasajero  pasajero)
{
    Nodo* nuevoNodo = new Nodo(pasajero);
    if (ListaVacia())
        Cabeza = nuevoNodo;
    else {
        Nodo* nodoActual = Cabeza;
        while (nodoActual->Siguiente != nullptr) {
            nodoActual = nodoActual->Siguiente;
        }
        nodoActual->Siguiente = nuevoNodo;
    }
}


void LinkedList::InsertarInicio(const Pasajero& pasajero)
{
    Nodo* nuevoNodo = new Nodo(pasajero);
    if (ListaVacia())
        Cabeza = nuevoNodo;
    else {
        nuevoNodo->Siguiente = Cabeza;
        Cabeza = nuevoNodo;
    }
}


Pasajero LinkedList::RemoverPrimero()
{
    if (ListaVacia())
        throw ListaVaciaException{};
    Pasajero resultado = Cabeza->Valor;
    Cabeza = Cabeza->Siguiente;
    return resultado;

}


Pasajero LinkedList::VerPrimero()
{
    if (ListaVacia())
        throw ListaVaciaException{};
    return Cabeza->Valor;


}


bool LinkedList::ListaVacia() const
{
    return Cabeza == nullptr;
}




Nodo::Nodo() :Siguiente(nullptr)
{
}

Nodo::Nodo(Pasajero pasajero) {

    Siguiente(nullptr), Valor(pasajero);
    {
    }
}

