
#include "Pasajero.h"
#include <iostream>
using namespace std;

class ListaVaciaException {};

class Nodo {
public:
    Nodo* Siguiente;
    Pasajero Valor;
    Nodo();
    Nodo(const Pasajero& pasajero);
};

class LinkedList
{
public:
	Nodo* Cabeza;
public:
	LinkedList();
	void InsertarFinal(Pasajero pasajero);
	void InsertarInicio(const Pasajero& pasajero);
	Pasajero RemoverPrimero();
	Pasajero VerPrimero();
	bool ListaVacia() const;
};


