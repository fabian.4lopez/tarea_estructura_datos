#pragma once
#include "PiladeAlumnos.h"
#include "Alumno.h"
#include <sstream>
#include "Nodo.h"


PiladeAlumnos::PiladeAlumnos() :
	Cabeza(nullptr) {
}


void PiladeAlumnos::Push(const Alumno& alumno)
{
	Nodo* nuevoNodo = new Nodo(alumno);
	if (PilaVacia())
		Cabeza = nuevoNodo;
	else {
		nuevoNodo->Siguiente = Cabeza;
		Cabeza = nuevoNodo;
	}
}

Alumno PiladeAlumnos::Pop()
{
	if (PilaVacia())
		throw ListaVaciaException{};
	Alumno resultado = Cabeza->Valor;
	Cabeza = Cabeza->Siguiente;
	return resultado;

}



Alumno PiladeAlumnos::Peek()
{
	if (PilaVacia())
		throw ListaVaciaException{};
	return Cabeza->Valor;
}




bool PiladeAlumnos::PilaVacia() const
{
	return Cabeza == nullptr;
}



Nodo::Nodo() :Siguiente(nullptr)
{
}

Nodo::Nodo(const Alumno& alumno) :
	Siguiente(nullptr), Valor(alumno)
{
}



