
#include <iostream>
#include <string>
#include "PiladeAlumnos.h"
class Alumno
{
private:
    std::string name;
    std::string apellido;
   
public:
    Alumno();
    Alumno(std::string nombre, std::string apellido);

    std::string getname() const;
    void setname(std::string nombre);
    std::string getapellido() const;
    void setapellido(std::string apellido);
};



